﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SqlSugar ;

namespace SugarFineUI.CodeTemplates.FineUIPro.Template
{
    public class FineuiAdd:FineuiBase
    {
        public static string GetCode(TableModel tb)
        {
            string code = string.Format(@"
<%@ Page Language=""C#"" AutoEventWireup=""true"" CodeBehind=""{0}Add.aspx.cs"" Inherits=""{1}.{2}{0}.Add"" ValidateRequest=""false""%>

<!DOCTYPE html>

<html>
<head runat=""server"">
    <meta http-equiv=""Content-Type"" content=""text/html; charset=utf-8"" />
    <title></title>
</head>
<body>
    <form id=""form1"" runat=""server"">
        <f:PageManager  runat=""server"" AutoSizePanelID=""form_Edit"" FormLabelWidth=""135px"" />
        <f:Form ID=""form_Edit"" ShowBorder=""false"" ShowHeader=""false"" runat=""server"" AutoScroll=""true""
            EnableCollapse=""true"" BodyPadding=""15px 15px"">
            <Rows>              
 {3}
            </Rows>
            <Toolbars>
                <f:Toolbar  runat=""server"" Position=""Bottom"">
                    <Items>
                        <f:ToolbarFill runat=""server"" />
                        <f:Button ID=""Button_save"" runat=""server"" ValidateForms=""form_Edit"" Text=""保存"" Icon=""SystemSaveNew""
                            OnClick=""Button_save_OnClick"">
                        </f:Button>    
                    </Items>
                </f:Toolbar>
            </Toolbars>
        </f:Form>       
    </form>  
</body>
</html>
", tb.TableName,tb.NamespaceStr, GetNamespace2Str(tb.Namespace2Str), GetFormRowCode(tb.Columns));
            return code;
        }

        private static string GetFormRowCode(List<DbColumnInfo> clms)
        {
            var list= clms.Where(p => p.IsPrimarykey == false).ToList();
            StringBuilder sb=new StringBuilder();
            for (int i = 0; i < list.Count; i++)
            {
                sb.AppendLine("                <f:FormRow runat=\"server\">");
                sb.AppendLine("                     <Items>");
                sb.AppendLine("                         "+ GetFormControl(list[i]));
                sb.AppendLine("                     </Items>");
                sb.AppendLine("                </f:FormRow>");
            }

            return sb.ToString();
        }

        private static string GetFormControl(DbColumnInfo clm)
        {
            string showStr = !clm.IsNullable  ? "Required=\"true\"  ShowRedStar=\"true\"" : string.Empty;//是否必填，0不可以为null，1可以为null
            string showName = GetComment(clm.ColumnDescription,clm.DbColumnName);
            string showDefault = clm.DefaultValue.Length == 0 ? string.Empty : clm.DefaultValue.Replace("(", string.Empty).Replace(")", string.Empty);
            switch (clm.DataType.ToLower())
            {
                case "bit":
                    return "<f:CheckBox ID=\"F_" + clm.DbColumnName + "\" runat=\"server\" Label=\"" + showName + "\"  /> ";
               
                case "datetime":
                    return "<f:DatePicker ID=\"F_" + clm.DbColumnName + "\" DateFormatString=\"yyyy-MM-dd HH:mm:ss\"  Label=\"" + showName + "\" runat=\"server\" " + showStr + " ></f:DatePicker> ";
              
                case "bigint"://break;
                case "smallint"://break;
                case "tinyint"://break;
                case "int":
                    return "<f:NumberBox ID=\"F_" + clm.DbColumnName + "\" runat=\"server\" Label=\"" + showName + "\" " + showStr + " Text=\"" + showDefault + "\" ></f:NumberBox> ";

                case "float":// break;
                case "money":// break;
                case "decimal":
                    return "<f:NumberBox ID=\"F_" + clm.DbColumnName + "\" runat=\"server\" Label=\"" + showName + "\" " + showStr + " Text=\"" + showDefault + " \" NoDecimal =\"false\"></f:NumberBox> ";

                default:
                    return "<f:TextBox ID=\"F_" + clm.DbColumnName + "\" runat=\"server\" Label=\"" + showName + "\" " + showStr + " ></f:TextBox> ";
            }
        }
        
    }
}
