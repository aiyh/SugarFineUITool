﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SqlSugar ;

namespace SugarFineUI.CodeTemplates.FineUIPro.Template
{
    public class FineuiView : FineuiBase
    {
        public static string GetCode(TableModel tb)
        {
            string code = string.Format(@"
<%@ Page Language=""C#"" AutoEventWireup=""true"" CodeBehind=""{2}View.aspx.cs"" Inherits=""{0}.{1}{2}.View"" %>

<!DOCTYPE html>

<html>
<head runat=""server"">
    <meta http-equiv=""Content-Type"" content=""text/html; charset=utf-8"" />
    <title></title>
</head>
<body>
    <form id=""form1"" runat=""server"">
        <f:PageManager runat=""server"" AutoSizePanelID=""form_Edit"" FormLabelWidth=""135px"" />
        <f:Form ID=""form_Edit"" ShowBorder=""false"" ShowHeader=""false"" runat=""server"" AutoScroll=""true""
            EnableCollapse=""true"" BodyPadding=""15px 15px"">
            <Rows>    
{3}
            </Rows>  
        </f:Form>
    </form>
</body>
</html>
", tb.NamespaceStr,
   GetNamespace2Str(tb.Namespace2Str),
   tb.TableName,
   GetItems(tb.Columns)
);
            return code;
        }

        private static string GetItems(List<DbColumnInfo> clms)
        {
            var list = clms.Where(p => p.IsPrimarykey == false).ToList();
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < list.Count; i++)
            {
                sb.AppendLine("                    <f:FormRow runat=\"server\">");
                sb.AppendLine("                         <Items>");
                sb.AppendLine("                             <f:Label ID =\"F_" + list[i].DbColumnName + "\" runat=\"server\" Label=\"" + GetComment(list[i].ColumnDescription, list[i].DbColumnName) + "\" />");
                sb.AppendLine("                         </Items>");
                sb.AppendLine("                    </f:FormRow>");
            }

            return sb.ToString();
        }
    }
}
