﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SqlSugar ;

namespace SugarFineUI.CodeTemplates
{
    public class FineuiModule
    {
        /// <summary>
        /// 控件类型
        /// </summary>
        public enum ModuleType
        {
            DropDownList = 1,
            CheckBoxList = 2,
            RadioButtonList = 3,

            KindEditor = 10
        }

        /// <summary>
        /// 根据类型名称 获取类型
        /// </summary>
        /// <param name="typeName"></param>
        /// <returns></returns>
        public static Type GetTypeByString(string typeName)
        {
            switch (typeName.ToLower())
            {
                case "system.boolean":
                    return Type.GetType("System.Boolean", true, true);
                case "system.byte":
                    return Type.GetType("System.Byte", true, true);
                case "system.sbyte":
                    return Type.GetType("System.SByte", true, true);
                case "system.char":
                    return Type.GetType("System.Char", true, true);
                case "system.decimal":
                    return Type.GetType("System.Decimal", true, true);
                case "system.double":
                    return Type.GetType("System.Double", true, true);
                case "system.single":
                    return Type.GetType("System.Single", true, true);
                case "system.int32":
                    return Type.GetType("System.Int32", true, true);
                case "system.uint32":
                    return Type.GetType("System.UInt32", true, true);
                case "system.int64":
                    return Type.GetType("System.Int64", true, true);
                case "system.uint64":
                    return Type.GetType("System.UInt64", true, true);
                case "system.object":
                    return Type.GetType("System.Object", true, true);
                case "system.int16":
                    return Type.GetType("System.Int16", true, true);
                case "system.uint16":
                    return Type.GetType("System.UInt16", true, true);
                case "system.string":
                    return Type.GetType("System.String", true, true);
                case "system.datetime":
                case "datetime":
                    return Type.GetType("System.DateTime", true, true);
                case "system.guid":
                    return Type.GetType("System.Guid", true, true);
                default:
                    return Type.GetType(typeName, true, true);
            }
        }


        /// <summary>
        /// 生成实体生成代码
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        public static string GetModelCode(List<DbColumnInfo> list)
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < list.Count; i++)
            {
                switch (list[i].DataType)
                {
                    case "bit":
                        sb.AppendLine("        ///<summary>");
                        sb.AppendLine("        ///" + list[i].ColumnDescription);
                        sb.AppendLine("        ///</summary>");
                        sb.AppendLine("        public bool " + (list[i].DbColumnName) + " { get; set; }");
                        sb.AppendLine("");
                        break;

                    case "datetime":
                        sb.AppendLine("        ///<summary>");
                        sb.AppendLine("        ///" + list[i].ColumnDescription);
                        sb.AppendLine("        ///</summary>");
                        sb.AppendLine("        public DateTime " + (list[i].DbColumnName) + " { get; set; }");
                        sb.AppendLine("");
                        break;

                    case "bigint":
                        sb.AppendLine("        ///<summary>");
                        sb.AppendLine("        ///" + list[i].ColumnDescription);
                        sb.AppendLine("        ///</summary>");
                        sb.AppendLine("        public Int64 " + (list[i].DbColumnName) + " { get; set; }");
                        sb.AppendLine("");
                        break;

                    case "smallint": //break;
                    case "tinyint": //break;
                    case "int":
                        sb.AppendLine("        ///<summary>");
                        sb.AppendLine("        ///" + list[i].ColumnDescription);
                        sb.AppendLine("        ///</summary>");
                        sb.AppendLine("        public int " + (list[i].DbColumnName) + " { get; set; }");
                        sb.AppendLine("");
                        break;

                    case "smallmoney": //break;
                    case "money": //break;
                    case "numeric": //break;
                    case "decimal":
                        sb.AppendLine("        ///<summary>");
                        sb.AppendLine("        ///" + list[i].ColumnDescription);
                        sb.AppendLine("        ///</summary>");
                        sb.AppendLine("        public Decimal " + (list[i].DbColumnName) + " { get; set; }");
                        sb.AppendLine("");
                        break;

                    case "float":
                        sb.AppendLine("        ///<summary>");
                        sb.AppendLine("        ///" + list[i].ColumnDescription);
                        sb.AppendLine("        ///</summary>");
                        sb.AppendLine("        public Double " + (list[i].DbColumnName) + " { get; set; }");
                        sb.AppendLine("");
                        break;

                    case "uniqueidentifier":
                        sb.AppendLine("        ///<summary>");
                        sb.AppendLine("        ///" + list[i].ColumnDescription);
                        sb.AppendLine("        ///</summary>");
                        sb.AppendLine("        public Guid " + (list[i].DbColumnName) + " { get; set; }");
                        sb.AppendLine("");
                        break;

                    default:
                        sb.AppendLine("        ///<summary>");
                        sb.AppendLine("        ///" + list[i].ColumnDescription);
                        sb.AppendLine("        ///</summary>");
                        sb.AppendLine("        public string " + (list[i].DbColumnName) + " { get; set; }");
                        sb.AppendLine("");
                        break;
                }

            }

            return sb.ToString();
        }
    }
}
