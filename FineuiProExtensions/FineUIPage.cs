﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Web;
using System.Web.Security;

namespace FineUIPro
{
    public class FineUIPage : System.Web.UI.Page
    {

        #region 初始化语言和皮肤

        protected override void OnInit(EventArgs e)
        {
            var pm = PageManager.Instance ;
            if (pm != null)
            {
                HttpCookie themeCookie = Request.Cookies["Theme"] ;
                if (themeCookie != null)
                {
                    string themeValue = themeCookie.Value ;

                    // 是否为内置主题
                    if (IsSystemTheme(themeValue))
                    {
                        pm.CustomTheme = String.Empty ;
                        pm.Theme = (Theme) Enum.Parse(typeof(Theme), themeValue, true) ;
                    }
                    else
                    {
                        pm.CustomTheme = themeValue ;
                    }
                }

                if (Constants.IS_BASE)
                {
                    pm.EnableAnimation = false ;
                }
            }

            base.OnInit(e) ;
        }

        private bool IsSystemTheme(string themeName)
        {
            string[] themes = Enum.GetNames(typeof(Theme)) ;
            foreach (string theme in themes)
            {
                if (theme.Equals(themeName,StringComparison.OrdinalIgnoreCase) )
                {
                    return true ;
                }
            }
            return false ;
        }

        #endregion


        #region Fineui公共方法 

        #region Alert 或 页面跳转

        /// <summary>
        /// 提示信息(弹层用),并指定父页面跳转
        /// </summary>
        /// <param name="message">内容</param>
        /// <param name="url"></param>
        protected void AlertInforAndRedirect(string message, string url)
        {
            FineUIPro.Alert aa = new FineUIPro.Alert() ;
            aa.AlertInforAndRedirect(message, url) ;
        }

        /// <summary>
        /// 提示信息(弹层用)
        /// </summary>
        /// <param name="message">内容</param>
        /// <param name="isPostBackReference">是否回发刷新页面</param>
        protected void AlertInfor(string message, bool isPostBackReference)
        {
            FineUIPro.Alert aa = new FineUIPro.Alert() ;
            aa.AlertInfor(message, isPostBackReference) ;
        }

        /// <summary>
        /// 提示信息（单页用 并刷新当前页）
        /// </summary>
        /// <param name="message">内容</param>
        /// <param name="isPostBackReference">是否回发刷新页面</param>
        protected void AlertInforBySingerPage(string message, bool isPostBackReference)
        {
            FineUIPro.Alert aa = new FineUIPro.Alert() ;
            aa.AlertInforBySingerPage(message, isPostBackReference) ;

        }

        /// <summary>
        /// 报错信息
        /// </summary>
        /// <param name="message">内容</param>
        /// <param name="isPostBackReference">是否回发刷新页面</param>
        protected void AlertError(string message, bool isPostBackReference)
        {
            FineUIPro.Alert aa = new FineUIPro.Alert() ;
            aa.AlertError(message, isPostBackReference) ;
        }

        /// <summary>
        /// 疑问信息
        /// </summary>
        /// <param name="message">内容</param>
        /// <param name="isPostBackReference">是否回发刷新页面</param>
        protected void AlertQuestion(string message, bool isPostBackReference)
        {
            FineUIPro.Alert aa = new FineUIPro.Alert() ;
            aa.AlertQuestion(message, isPostBackReference) ;
        }

        /// <summary>
        /// 警告信息
        /// </summary>
        /// <param name="message">内容</param>
        /// <param name="isPostBackReference">是否回发刷新页面</param>
        protected void AlertWarning(string message, bool isPostBackReference)
        {
            FineUIPro.Alert aa = new FineUIPro.Alert() ;
            aa.AlertWarning(message, isPostBackReference) ;
        }

        /// <summary>
        /// 子页面关闭并在父业面跳转
        /// </summary>
        /// <param name="url"></param>
        protected void ParentRedirect(string url)
        {

            FineUIPro.Alert aa = new FineUIPro.Alert() ;
            aa.ParentRedirect(url) ;
        }

        #endregion

        #region Notify 消息框

        /// <summary>
        /// 警告消息框
        /// </summary>
        /// <param name="message">警告信息</param>
        /// <param name="js">然后执行的js代码</param>
        public void NotifyInformation(string message, string js = null)
        {
            Notify nf = new Notify() ;
            nf.GetShowNotify() ;
            nf.Message = message ;
            nf.MessageBoxIcon = MessageBoxIcon.Information ;
            if (!string.IsNullOrEmpty(js)) nf.HideScript = js ;
            nf.Show() ;
        }

        /// <summary>
        /// 警告消息框
        /// </summary>
        /// <param name="message">警告信息</param>
        /// <param name="js">然后执行的js代码</param>
        public void NotifyWarning(string message, string js = null)
        {
            Notify nf = new Notify() ;
            nf.GetShowNotify() ;
            nf.Message = message ;
            nf.MessageBoxIcon = MessageBoxIcon.Warning ;
            if (!string.IsNullOrEmpty(js)) nf.HideScript = js ;
            nf.Show() ;
        }

        /// <summary>
        /// 错误消息框
        /// </summary>
        /// <param name="message">错误信息</param>
        /// <param name="js">然后执行的js代码</param>
        public void NotifyError(string message, string js = null)
        {
            Notify nf = new Notify() ;
            nf.GetShowNotify() ;
            nf.Message = message ;
            nf.MessageBoxIcon = MessageBoxIcon.Error ;
            if (!string.IsNullOrEmpty(js)) nf.HideScript = js ;
            nf.Show() ;
        }

        #endregion

        #region grid


        /// <summary>
        /// 获取选中行的主id
        /// </summary>
        /// <param name="grid">FineUIPro.Grid</param>
        /// <param name="keyNumIndex">第几个主键（从0开始）</param>
        /// <returns></returns>
        public static string GetDataKeysBySelectedRow(FineUIPro.Grid grid, int keyNumIndex = 0)
        {
            return grid.GetDataKeysBySelectedRow(keyNumIndex) ;
        }

        #endregion

        #endregion
    }
}
