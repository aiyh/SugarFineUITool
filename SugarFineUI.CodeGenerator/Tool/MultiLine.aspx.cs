﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FineUIPro;

namespace SundayOA.Web.Tool
{
    public partial class MultiLine : FineUIPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btn_showCode_OnClick(object sender, EventArgs e)
        {
            string[] data = txa_old.Text.Split(Environment.NewLine.ToCharArray());

            StringBuilder sb = new StringBuilder();
            if (cb_Trim.Checked)
                for (int i = 0; i < data.Length; i++)
                    sb.Append(data[i].Trim());
            else
                for (int i = 0; i < data.Length; i++)
                    sb.Append(data[i]);

            txa_new.Text = sb.ToString();
        }


    }
}