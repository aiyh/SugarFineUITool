﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SugarFineUI.Framework.Uitility;
using FineUIPro;


namespace SugarFineUI.CodeGenerator.Tool
{
    public partial class Mosaic : FineUIPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btn_showCode_OnClick(object sender, EventArgs e)
        {
            //验证
            string pattern = @"\{\d\}"; //包含{0} {1} 等
           
            if (Regex.Matches(txb_Expression.Text, pattern).Count==0)
            {
                Alert.Show("表达式必须含有'{0}' '{1}' 表达式等!");
                return;
            }

            try
            {
                string[] data = txa_old.Text.Split(Environment.NewLine.ToCharArray());

                StringBuilder sb=new StringBuilder();
                for (int i = 0; i < data.Length; i++)
                {
                    sb.Append(GetExchangeString( data[i].Trim()));
                }

                txa_new.Text = sb.ToString();
            }
            catch (FormatException formatEx)
            {
                Alert.Show("注意输出‘{’或‘}’需要转义，既‘{{’或‘}}’。注意分组字符串个数。");
                return;
            }
        }

        private string GetExchangeString(string addStr)
        {
            if (addStr.Trim().Length == 0) return string.Empty;
          
            return string.Format(@txb_Expression.Text, StringHelper.ReplaceExcessSpace(addStr).Split(' '));
        }
    }
}