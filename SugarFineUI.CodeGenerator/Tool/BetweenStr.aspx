﻿<%@ Page Language="C#" AutoEventWireup="True" CodeBehind="BetweenStr.aspx.cs" Inherits="SugarFineUI.CodeGenerator.BetweenStr" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>获取两个字符串中间的字符串</title>
</head>
<body>
    <form id="form1" runat="server">
        <f:PageManager runat="server" AutoSizePanelID="form_Edit" FormLabelWidth="140" />

        <f:Form ID="form_Edit" ShowBorder="True" ShowHeader="False" Title="ABCD，第1个字符串AB,第2个字符串例D,获取C " runat="server" AutoScroll="true"
            EnableCollapse="false" BodyPadding="15px 15px">
            <Rows>
                <f:FormRow runat="server" ColumnWidth="50% 50%">
                    <Items>
                        <f:TextBox runat="server" ID="txb_A" Label="第1个字符串" ShowRedStar="True" />
                        <f:TextBox runat="server" ID="txb_B" Label="第2个字符串" />

                    </Items>
                </f:FormRow>



                <f:FormRow runat="server">
                    <Items>
                        <f:TextArea runat="server" ID="txa_old" Height="300px" Required="True" ShowRedStar="True" Label="批量内容"/>
                    </Items>
                </f:FormRow>

                <f:FormRow runat="server">
                    <Items>
                        <f:Button runat="server" Text="转到上方↑" OnClientClick=" codeUP();return ;" EnableAjax="False" EnablePostBack="False" DisableControlBeforePostBack="False" />

                    </Items>
                </f:FormRow>
                <f:FormRow runat="server">
                    <Items>
                        <f:TextArea runat="server" ID="txa_new" Height="300px" Label="新内容" Readonly="True" />
                    </Items>
                </f:FormRow>

            </Rows>

            <Toolbars>
                <f:Toolbar runat="server" Position="Top">
                    <Items>
                        <f:Button runat="server" ValidateForms="form_Edit" ID="btn_showCode" Text="提取值" Icon="Html" OnClick="btn_showCode_OnClick" />   
                        <f:ToolbarSeparator runat="server" />
                        <f:RadioButtonList ID="rbl_line" Label="提取方式" runat="server" LabelAlign="Right" LabelWidth="80px">
                            <f:RadioItem Text="逐行提取" Value="1" Selected="True"/>
                            <f:RadioItem Text="全文提取" Value="2" />
                        </f:RadioButtonList>
                        <f:ToolbarSeparator runat="server" />
                        <f:CheckBox runat="server" ID="cb_IgnoreCase" Label="是否忽略大小写" Checked="True" LabelAlign="Right" LabelWidth="120px"/>
                        <f:ToolbarSeparator runat="server" />
                        <f:ToolbarText runat="server" Text="ABCD，第1个字符串AB,第2个字符串例D,获取C" />
                    </Items>
                </f:Toolbar>
            </Toolbars>
        </f:Form>
    </form>
</body>
</html>
<script>
    function codeUP() {
        F('<%=txa_old.ClientID%>').setValue(F('<%=txa_new.ClientID%>').getValue());
        F('<%=txa_new.ClientID%>').setValue("");
    }
</script>

