﻿<%@ Page Language="C#" AutoEventWireup="True" CodeBehind="Xpath.aspx.cs" Inherits="SugarFineUI.CodeGenerator.Tool.Xpath" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>拼接代码</title>
</head>
<body>
    <form id="form1" runat="server">
        <f:PageManager runat="server" AutoSizePanelID="form_Edit" />

        <f:Form ID="form_Edit" ShowBorder="True" ShowHeader="False" Title=" " runat="server" AutoScroll="true"
                EnableCollapse="false" BodyPadding="15px 15px">
            <Rows>
                <f:FormRow runat="server" ColumnWidth="50% 50%" >
                    <Items>
                        <f:TextBox runat="server" ID="txb_Expression" Label="表达式"   Required="True" ShowRedStar="True"  />
                       
                    </Items>
                </f:FormRow>

                

                <f:FormRow runat="server">
                    <Items>
                        <f:TextArea runat="server" ID="txa_old" Height="300px" Required="True" ShowRedStar="True" Label="Html"/>
                    </Items>
                </f:FormRow>

            
                <f:FormRow runat="server">
                    <Items>
                        <f:TextArea runat="server" ID="txa_new"  Height="300px" Readonly="True" Label="匹配内容"/>
                    </Items>
                </f:FormRow>

            </Rows>

            <Toolbars>
                <f:Toolbar runat="server" Position="Top">
                    <Items>
                   
                        <f:Button runat="server" ValidateForms="form_Edit" ID="btn_showCode" Text="测试匹配" Icon="Html" OnClick="btn_showCode_OnClick" />
                        <f:ToolbarSeparator runat="server" />
                   
                        <f:Button runat="server" ValidateForms="form_Edit" ID="btn_delPar" Text="删除注释" Icon="Html" OnClick="btn_delPar_OnClick" />
                        <f:ToolbarSeparator runat="server" />
                       <f:ToolbarText runat="server" Text="HtmlAgilityPack表达式测试工具"/>
                    </Items>
                </f:Toolbar>
            </Toolbars>
        </f:Form>
    </form>
</body>
</html>
<script>
    function codeUP() {
        F('<%=txa_old.ClientID%>').setValue(F('<%=txa_new.ClientID%>').getValue());
        F('<%=txa_new.ClientID%>').setValue("");
    }
</script>