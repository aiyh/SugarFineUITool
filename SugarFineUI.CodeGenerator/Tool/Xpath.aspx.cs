﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FineUIPro;
using HtmlAgilityPack ;


namespace SugarFineUI.CodeGenerator.Tool
{
    public partial class Xpath : FineUIPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btn_showCode_OnClick(object sender, EventArgs e)
        {
           
         
                HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
                doc.LoadHtml(txa_old.Text);
               

                HtmlNodeCollection nodes= doc.DocumentNode.SelectNodes(txb_Expression.Text);

                if (nodes == null)
                {
                    txa_new.Text = "===================匹配不到==================";
                    return;
                }

                StringBuilder sb=new StringBuilder();
                HtmlNode node ;
                for (int i = 0; i < nodes.Count; i++)
                {
                    node= nodes[i] ;
                    sb.AppendLine("InnerHtml:"+node.InnerHtml) ;
                    sb.AppendLine("InnerText:" + node.InnerText) ;
                    sb.AppendLine("OuterHtml:" + node.OuterHtml) ;
                    sb.AppendLine("XPath:" + node.XPath) ;
                    sb.AppendLine("===================我是分隔符===================") ;
                }

                txa_new.Text = sb.ToString() ;
       
        }

      

        protected void btn_delPar_OnClick(object sender, EventArgs e)
        {
            HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
            doc.LoadHtml(txa_old.Text);

            foreach (HtmlNode comment in doc.DocumentNode.SelectNodes("//comment()"))
            {
                //删除注释的html代码
                comment.ParentNode.RemoveChild(comment);
            }

            string newTableHtml = doc.DocumentNode.InnerHtml;
            txa_new.Text = newTableHtml ;
        }
    }
}