﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DbConfig.aspx.cs" Inherits="SugarFineUI.CodeGenerator.DbConfig" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>数据库配置</title>
</head>
<body>
    <form id="form1" runat="server">
        <f:PageManager runat="server" AutoSizePanelID="panel2" />

        <f:Panel runat="server" ID="panel2" RegionPosition="Right" RegionSplit="true" EnableCollapse="true"
            Width="200px" Title="数据库" Layout="VBox" ShowBorder="False" ShowHeader="False" BodyPadding="5px" IconFont="_PullRight">
            <items>
                <f:Panel runat="server" ShowBorder="False" ShowHeader="False" Layout="Column">
                    <Items>  
                        <f:TextBox ID="ttbDB" runat="server" ShowLabel="false" EmptyText="数据库连接" ColumnWidth="100%" />
                        <f:Button runat="server" Icon="DatabaseLink" ID="btn_DBlink" Size="Normal" OnClick="btn_DBlink_OnClick"></f:Button>
                    </Items>
                </f:Panel>
                <f:TwinTriggerBox ID="ttbSearchTableName" runat="server" ShowLabel="false" EmptyText="表名搜索" OnTrigger1Click="ttbSearchTableName_OnTrigger1Click" OnTrigger2Click="ttbSearchTableName_OnTrigger2Click"
                                  Trigger1Icon="Clear" Trigger2Icon="Search" ShowTrigger1="false" />
                <f:Panel runat="server" Layout="HBox" ShowBorder="False" ShowHeader="False">
                    <Items>
                        <f:Button runat="server" Text="全选" Icon="TableMultiple" EnablePostBack="False" OnClientClick="setallcheck(true);" MarginLeft="2px"/>
                        <f:Button runat="server" Text="都不选" Icon="TableDelete" EnablePostBack="False" OnClientClick="setallcheck(false);" MarginLeft="2px"/>
                    </Items>
                </f:Panel>
                <f:CheckBoxList runat="server" ID="cbl_tables" ColumnNumber="1"></f:CheckBoxList>
            </items>
        </f:Panel>
        <f:Button runat="server" ID="btn_RefreshTables" Hidden="True" OnClick="btn_RefreshTables_OnClick"></f:Button>
    </form>
</body>
</html>
<script>
    function RefreshTables() {
        __doPostBack('btn_RefreshTables', '');
    }

    //设置全选或全不选
    function setallcheck(isCheck) {
        var array = F('<%=cbl_tables.ClientID%>').items;
        array.forEach(function (item) {
            item.setValue(isCheck);
        });
    }

    function GetTables() {
        var selectedValues = F('<%=cbl_tables.ClientID%>').getValue().join(',');
        return selectedValues;
    }
</script>